# Notification Test Task

This is a test task that implement notification service. the notification service need to save
the notification first on the database then send this notification to the required provider.

## Outline

 - [Libraries](#Libraries)
 - [Installation](#installation)
 - [Dependencies](#Dependencies)
 - [Swagger](#Swagger)
 - [Steps to run tests](#Test)
 - [Architecture](#Architecture)

## Libraries

This project is using several libraries and frameworks:

 - [mongoose](https://www.npmjs.com/package/mongoose) - Database
 - [sqs-consumer](https://www.npmjs.com/package/sqs-consumer) - SQS Consumer
 - [sqs-producer](https://www.npmjs.com/package/sqs-producer) - SQS Producer
 - [joi](https://www.npmjs.com/package/joi) - Joi Validator
 - [mocha](https://www.npmjs.com/package/mocha) - Testing
 - [chai](https://www.npmjs.com/package/chai) - Testing

## Installation

### Dependencies

Make sure you have Docker:

To run project 

```bash
$ docker-compose build
```

Then type into the terminal:

```bash
$ docker-compose up
```

Start using APIs on swagger steps:

1- Open Swagger http://localhost:3000/api-docs  

2- Create user first by using API POST method /users

3- Send notification by passing user_id created to receiver array in API POST method /notification

4- You should see on the terminal  Email with subject: Welcome,
      message: Welcome to Swvl,
   to email: almalkymomen@gmail.com


## Swagger

This App is using swagger as a documentation for the APIs you can open swagger on following link
http://localhost:3000/api-docs


## Test
1- Change ENV NODE_ENV on Docker file on both projects (notification_app/Dockerfile && consumer_app/Dockerfile) to test

ENV NODE_ENV=test  

for consumer_app/Dockerfile
comment on the following by adding # so it will be like this:<br />
#CMD ["npm", "run", "consumer"]<br />
and comment out the following by removing # so it will be like this: <br />
CMD [ "npm", "test"]

and for notification_app/Dockerfile
comment on the following by adding # so it will be like this:<br />
#CMD ["npm", "run", "start"]<br />
and comment out the following by removing # so it will be like this:<br />
CMD [ "npm", "test"]

2- Build docker and run docker by these commands

```bash
$ docker-compose build
```

Then type into the terminal:

```bash
$ docker-compose up
```

## Architecture

The idea was to make the sqs consumer and notification providers on a separate
to be able to be deployed independently and to be scaled independently.

The architecture of the app are based on two nodes:

### Notification App (Service)

This app handle the request to save and retrieve users and notifications and responsible to handle MongoDB request (handle the request from and to user).

### Provider (Consumer Queue) (Service)


This service contain queue consumer and notification providers. consumer will consume items 
added on the queue and send the notification.


### Request Methods

As a rule of thumb, the request methods is as follows:

|Method|Description|link|
| ------ | ------ | ----- |
|get|Get list of users|http://localhost:3000/users|
|post|Create new user|http://localhost:3000/users|
|get|Get list of notifications for user|http://localhost:3000/users/notifications|
|get|Get list of notifications|http://localhost:3000/notifications|
|post|Create new notification|http://localhost:3000/notifications|

