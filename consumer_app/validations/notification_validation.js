const Joi = require("joi");
const constants = require("../helpers/constants");

const new_notification_validation = (data) => {
  const schema = Joi.object({
    provider: Joi.string().required().valid(...constants.providers),
    type: Joi.string().required().valid(...constants.types),
    subject: Joi.string().required().min(1),
    message: Joi.string().required().min(1),
    receiver: Joi.array().min(1) .messages({
      'array.empty': `"receiver" cannot be an empty field, hint: create user first.`,
      'array.min': `"receiver" must contain at least 1 items, hint: create user first.`,
      'any.required': `"receiver" is a required field`
    }),
  });

  return schema.validate(data, { allowUnknown: true });
};

module.exports.new_notification_validation = new_notification_validation;
