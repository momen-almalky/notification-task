/**
 * Returns an array with arrays of the given size.
 *
 * @param chunkArray array to split
 * @param chunkSize size of every group
 */
module.exports.getChunkArray = function (chunk_array, chunk_size){
  var results = [];
  
  while (chunk_array.length) {
      results.push(chunk_array.splice(0, chunk_size));
  }
  
  return results;
}
