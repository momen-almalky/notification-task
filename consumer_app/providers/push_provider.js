const { getChunkArray } = require("../helpers/array_helper");
require("dotenv").config();

const RPM = process.env.EMAIL_RPM; // Provider Request Per Minute
const waiting_time = 60 * 1000; // interval time(1 min in millisecond)
let interval; // use to clear interval
let chunks = []; // chunks of receivers

/**
 * divide receivers into chunks and apply requests per seconds
 * @param notification
 * @return boolean
 */
module.exports.send = function (notification) {
  chunks.push(...getChunkArray(notification.receiver, RPM));
  // start the first chunk without waiting
  sendMessage(notification);

  if (!interval) {
    interval = setInterval(sendMessage, waiting_time, notification);
  }
  
  return true;
};


/**
 * send notification
 * @param notification
 */
function sendMessage(notification) {
  if (!chunks.length) {
    clearInterval(interval);
    return;
  }
  receivers = chunks.pop();
  receivers.forEach((element) => {
    console.log(
      `Push notification with subject: ${notification.subject},
      message: ${notification.message},
      to user token: ${element.id_token}`
    );
  });
}
