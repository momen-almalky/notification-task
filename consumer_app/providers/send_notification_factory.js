const sms = require("./sms_provider");
const push_provider = require("./push_provider");
const email_provider = require("./email_provider");

/**
 * send notification
 * @param notification
 */
module.exports.sendNotification = function(notification) {
  let response;
  switch (notification.provider) {
    case "sms":
      response = sms.send(notification);
      break;
    case "push_notification":
      response = push_provider.send(notification);
      break;
    case "email":
      response = email_provider.send(notification);
      break;
  }
  return response;
}
