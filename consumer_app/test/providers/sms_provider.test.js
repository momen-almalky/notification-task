const sms = require("../../providers/sms_provider");
const sms_sample = require("../samples/sms_sample");
const chai = require("chai");
const expect = chai.expect;

describe("providers/sms_provider", () => {
  describe("send", () => {
    it("should return true", () => {
      expect(sms.send(sms_sample)).to.be.true;
    });
  });
});
