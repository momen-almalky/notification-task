const {sendNotification} = require("../../providers/send_notification_factory");
const {sms} = require("../samples/notification_sample");
const {email} = require("../samples/notification_sample");
const {push} = require("../samples/notification_sample");
const chai = require('chai')
const expect = chai.expect

describe("providers/send_notification_factory", () => {

  describe("send", () => {
    
    it("should return true", () => {
      expect(sendNotification(email)).to.be.true;
    });
  
    it("should return true", () => {
        expect(sendNotification(sms)).to.be.true;
    });

    it("should return true", () => {
        expect(sendNotification(push)).to.be.true;
    });
  });

});
