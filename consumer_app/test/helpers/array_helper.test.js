const chai = require("chai");
const expect = chai.expect;
const { getChunkArray } = require("../../helpers/array_helper");


describe("providers/array_helper", () => {

  describe("getChunkArray", () => {
    it("should return ", () => {
      expect(getChunkArray([1,2,3,4,5,6,7,8,9,10],4)).eql([[1,2,3,4],[5,6,7,8],[9,10]]);
    });
    
  });

});
