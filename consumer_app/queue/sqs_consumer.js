const { Consumer } = require("sqs-consumer");
const AWS = require("aws-sdk");
const { sendNotification } = require("../providers/send_notification_factory");
const {
  new_notification_validation,
} = require("../validations/notification_validation");
require("dotenv").config();

/**
 * SQS consumer
 *
 */
module.exports.handle_messages = function () {
  AWS.config.update({
    region: process.env.SQS_REGION,
    accessKeyId: process.env.SQS_ACCESS_KEY_ID,
    secretAccessKey: process.env.SQS_SECRET_ACCESS_KEY,
  });

  const consumer = Consumer.create({
    queueUrl: process.env.SQS_QUEUE_URL,
    handleMessage: async (message) => {
      notification = JSON.parse(message.Body);
      const { error } = new_notification_validation(notification);
      if (error) {
        console.log({ error: error.details[0].message });
      }

      sendNotification(notification);
    }
  });

  consumer.on("error", (err) => {
    console.error("err.message");
    console.error(err.message);
  });

  consumer.on("processing_error", (err) => {
    console.error("processing_error");

    console.error(err.message);
  });

  consumer.start();
};
