const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const sqs_consumer = require("./queue/sqs_consumer");

dotenv.config();

const app = express();
const server = require("http").createServer(app);
app.use(bodyParser.json());

sqs_consumer.handle_messages();

const port = process.env.CONSUMER_PORT || 4000;
server.listen(port, () => {
  console.log("Consumer Queue is up on port " + port);
});
