const { Producer } = require("sqs-producer");
require("dotenv").config();

/**
 *
 * @param notification
 */
module.exports.sendNotificationProducer = async function (notification) {
  // create simple producer
  const producer = Producer.create({
    queueUrl: process.env.SQS_QUEUE_URL,
    region: process.env.SQS_REGION,
    accessKeyId: process.env.SQS_ACCESS_KEY_ID,
    secretAccessKey: process.env.SQS_SECRET_ACCESS_KEY,
  });

  response = await producer.send([
    {
      id: notification._id.toString(),
      body: JSON.stringify(notification),
    },
  ]);
  
  return response;
};
