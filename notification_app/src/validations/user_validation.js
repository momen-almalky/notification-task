const Joi = require("joi");
const constants = require("../helpers/constants");

const new_user_validation = (data) => {
  const schema = Joi.object({
    user_name: Joi.string().min(1).required(),
    email: Joi.string().min(6).email().required(),
    phone: Joi.string()
      .min(6)
      .required(),
    id_token: Joi.string().min(1).required(),
  });

  return schema.validate(data);
};

const get_user_validation = (data) => {
  const schema = Joi.object({
    user_id: Joi.required(),
  });

  return schema.validate(data);
};
module.exports.new_user_validation = new_user_validation;
module.exports.get_user_validation = get_user_validation;
