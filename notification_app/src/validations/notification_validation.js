const Joi = require("joi");
const constants = require("../helpers/constants");

const new_notification_validation = (data) => {
  const schema = Joi.object({
    provider: Joi.string().required().valid(...constants.providers),
    receiver: Joi.array().min(1),
    type: Joi.string().required().valid(...constants.types),
    subject: Joi.string().required().min(1),
    message: Joi.string().required().min(1),

  });

  return schema.validate(data);
};

module.exports.new_notification_validation = new_notification_validation;
