const mongoose = require("mongoose");
require("dotenv").config();

function connect() {
  return new Promise((resolve, reject) => {
    if (process.env.NODE_ENV === "test") {

        mongoose
          .connect(process.env.TEST_DATABASE, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true
          })
          .then((res, err) => {
            if (err) return reject(err);
            resolve();
          });
    } else {
      mongoose
        .connect(process.env.DATABASE, {
          useNewUrlParser: true,
          useCreateIndex: true,
          useFindAndModify: false,
          useUnifiedTopology: true
        })
        .then((res, err) => {
          if (err) return reject(err);
          resolve();
        });
    }
  });
}

function close() {
  return mongoose.disconnect();
}

module.exports = { connect, close };
