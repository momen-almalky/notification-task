const mongoose = require("mongoose");
const constants = require("../helpers/constants");

var notificationSchema = new mongoose.Schema(
  {
    provider: {
      type: String,
      required: true,
      enum: constants.providers,
    },
    receiver: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    type: {
      type: String,
      required: true,
      enum: constants.types,
    },
    subject: {
      type: String,
      required: true,
    },
    message: {
      type: String,
    },
  },
  { timestamps: true }
);

notificationSchema.pre("save", async function (next) {
  const now = new Date();

  if (!this.created_at) {
    this.created_at = now;
  }

  this.updated_at = now;

  next();
});

const Notification = mongoose.model("Notification", notificationSchema);
module.exports = Notification;
