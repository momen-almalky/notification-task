const mongoose = require("mongoose");
const validator = require('validator');

var userSchema = new mongoose.Schema(
  {
    user_name: {
      type: String,
      min: [5, "Too short, min is 5 characters"],
      max: [50, "Too long, max is 32 characters"],
      required: true,
      trim: true,
    },
    email: {
      type: String,
      min: [5, "Too short, min is 5 characters"],
      max: [32, "Too long, max is 32 characters"],
      unique: true,
      lowercase: true,
      required: "Email is required",
      match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
    },
    phone: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isMobilePhone(value, 'any')) {
                throw new Error('phone is invalid')
            }
        }
    },
    id_token: {
      type: String,
      unique: true,
      required: true,
    },
  },
  { timestamps: true }
);

userSchema.pre("save", async function (next) {
  const now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now;
  next();
});

const User = mongoose.model("User", userSchema);
module.exports = User;
