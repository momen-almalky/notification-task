const express = require("express");
const Notification = require("../models/notification");
const { sendNotificationProducer } = require("../queues/producer/producer");
const {new_notification_validation} = require("../validations/notification_validation");

const router = new express.Router();


/**
 * @api {get} /notifications get notification list
 * @apiName get notification list
 * @apiGroup notifications
 *
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.get("/notifications", async (req, res) => {
  try {
    const notifications = await Notification.find({});
    res.status(200).send({ notifications: notifications });
  } catch (err) {
    console.error(err);
    res.status(400).send({ err: err });
  }
});


/**
 * @api {post} /notifications Create notification
 * @apiName Create new notification
 * @apiGroup notifications
 *
 * @apiParam  {String} provider
 * @apiParam  {Array} [ObjectID] receiver
 * @apiParam  {String} type
 * @apiParam  {String} subject
 * @apiParam  {String} message
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/notifications", async (req, res) => {
  try {
    const { body } = req;

    const { error } = new_notification_validation(body);

    if (error) {
      return res.status(422).send({ message: error.details[0].message });
    }

    let notification = new Notification({ ...body });
    await notification.save();

    if (notification.receiver.length == 0) {
      res.status(200).send({ message: "Receiver is empty, create user first!" });
    }

    notification = await Notification.findById(notification._id).populate("receiver");
    
    response = await sendNotificationProducer(notification);

    if (response.length == 0) {
      res.status(400).send({ message: "notification was not sent!" });
    }

    res.status(200).send({ message: "notification was sent!" });
  } catch (err) {
    console.error(err);
    res.status(400).send({ err: err });
  }
});

module.exports = router;
