const express = require("express");
const User = require("../models/user");
const Notification = require("../models/notification");
const {
  get_user_validation,
  new_user_validation,
} = require("../validations/user_validation");

const router = new express.Router();


/**
 * @api {get} /users/notifications get user list
 * @apiName get user list
 * @apiGroup users
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.get("/users", async (req, res) => {
  try {
    const users = await User.find({});

    res.status(200).send({ users: users });
  } catch (err) {
    res.status(400).send({ err: err });
  }
});


/**
 * @api {post} /users Create user
 * @apiName Create new user
 * @apiGroup users
 *
 * @apiParam  {String} username
 * @apiParam  {String} Email
 * @apiParam  {String} Phone number
 * @apiParam  {String} ID Token
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/users", async (req, res) => {
  try {
    const { body } = req;

    const { error } = new_user_validation(body);

    if (error) {
      return res.status(422).send({ message: error.details[0].message });
    }

    if (await User.findOne({ email: body.email })) {
      return res.status(400).json({ message: "this email used before" });
    }

    const user = new User({ ...body });
    await user.save();
    res.status(200).send({ user });
  } catch (err) {
    console.error(err);
    res.status(400).send({ err: err });
  }
});


/**
 * @api {get} /users/notifications get notification list
 * @apiName get notification list
 * @apiGroup users
 *
 * @apiParam  {String} [ObjectID] user_id
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.get("/users/notifications", async (req, res) => {
  try {
    const { query } = req;

    const { error } = get_user_validation(query);

    if (error) {
      return res.status(422).send({ message: error.details[0].message });
    }

    if (!(await User.findOne({ _id: query.user_id }))) {
      return res.status(400).json({ message: "User Not Found" });
    }
    const notifications = await Notification.find({
      receiver: query.user_id,
    });
    res.status(200).send({ notifications: notifications });
  } catch (err) {
    console.error(err);
    res.status(400).send({ err: err });
  }
});

module.exports = router;
