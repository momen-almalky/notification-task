const chai = require("chai");
const expect = chai.expect;
const user_sample = require("../samples/user_sample");
const request = require("supertest");
database = require("../../src/db/mongoose");
server = require("../../index");

describe("user router", () => {

  before((done) => {
    database.connect()
      .then(() => done())
      .catch((err) => done(err));
  })

  after((done) => {
    database.close()
      .then(() => done())
      .catch((err) => done(err));
  })

  describe("POST /users", () => {
    it("should create user in database", async () => {
      const res = await request(server).post("/users").send(user_sample);
      expect(res.status).eql(200);
    });
  });

  describe("GET /users", () => {
    it("should list users in database", async () => {
      const res = await request(server).get("/users");
      expect(res.status).eql(200);
    });
  });
});
