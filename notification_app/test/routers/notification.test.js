const chai = require("chai");
const expect = chai.expect;
const notification_sample = require("../samples/notification_sample");
const request = require("supertest");
database = require("../../src/db/mongoose");
server = require("../../index");

describe("Notification router", () => {

  before((done) => {
    database.connect()
      .then(() => done())
      .catch((err) => done(err));
  })

  after((done) => {
    database.close()
      .then(() => done())
      .catch((err) => done(err));
  })

  describe("POST /notifications", () => {
    it("should create notification in database", async () => {
      const res = await request(server)
        .post("/notifications")
        .send(notification_sample);
      expect(res.status).eql(200);
    });
  });

  describe("GET /notifications", () => {
    it("should list notifications in database", async () => {
      const res = await request(server).get("/notifications");
      expect(res.status).eql(200);
    });
  });
});
