const express = require("express");
mongoose = require("./src/db/mongoose");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./notification_swagger.json");

const user_router = require("./src/routers/user");
const notification_router = require("./src/routers/notification");

dotenv.config();

const app = express();
const server = require("http").createServer(app);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());
app.use(user_router);
app.use(notification_router);

console.log(
  "Open http://localhost:3000/api-docs to explore swagger and make requests"
);

mongoose.connect().then(() => {
  const port = process.env.PORT || 3000;
  server.listen(port, () => {
    console.log("Server is up on port " + port);
  });
});

module.exports = server;
